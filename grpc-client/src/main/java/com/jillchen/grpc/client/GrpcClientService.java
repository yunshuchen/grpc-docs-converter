package com.jillchen.grpc.client;


import com.google.protobuf.ByteString;
import com.jillchen.grpc.libs.*;
import com.jillchen.grpc.client.vo.InterruptProjectVo;
import com.jillchen.grpc.client.vo.InterruptStatusVo;
import io.grpc.Channel;
import io.grpc.netty.NegotiationType;
import io.grpc.netty.NettyChannelBuilder;
import net.devh.springboot.autoconfigure.grpc.client.GrpcClient;
import org.apache.commons.io.FileUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;


@Service
public class GrpcClientService {

    //两种获取Channel方式
    //方法一
    @GrpcClient("docs-grpc-server")
    private Channel serverChannel;

    public String sendMessage(String name) {
        GreeterGrpc.GreeterBlockingStub stub = GreeterGrpc.newBlockingStub(serverChannel);
        GreeterOuterClass.HelloReply response = stub.sayHello(GreeterOuterClass.HelloRequest.newBuilder().setName(name).build());
        return response.getMessage();
    }

    /*public boolean Convert(String FileName) {
        try {
            //File2ByteArray2ByteString
            ByteString byteString = ByteString.copyFrom(FileUtils.readFileToByteArray(new File(FileName)));

            DocsConvertGrpc.DocsConvertBlockingStub stub= DocsConvertGrpc.newBlockingStub(serverChannel);
            Convert.ResponseFileStreamReply response = stub.convert(Convert.RequestFileStream.newBuilder().setArrs(byteString).build());

            ByteString res_bs= response.getArrs();
            //ByteString2ByteArray
            byte[]  res_byte=res_bs.toByteArray();
            //ByteArray2File
            FileOutputStream fileOuputStream = new FileOutputStream("C:\\output\\grpc-pdf.pdf");
            fileOuputStream.write(res_byte);
            fileOuputStream.close();

        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return true;
    }*/

    //方法二
    @Value("${grpc.client.docs-grpc-server.port}")
    private Integer port;

    private String host = "0.0.0.0";

    public List<InterruptStatusVo> getResult(String building, String park, String region) {
        //通过端口号去获取
        Channel channel = NettyChannelBuilder.forAddress(host, port)
                .negotiationType(NegotiationType.PLAINTEXT)
                .build();

        InterruptGrpc.InterruptBlockingStub stub  =  InterruptGrpc.newBlockingStub(channel);
        InterruptStatus.RequestParam.Builder param = InterruptStatus.RequestParam.newBuilder();
        if(Objects.nonNull(building)){
            param.setLocationBuilding(building);
        }
        if(Objects.nonNull(park)){
            param.setLocationPark(park);
        }
        if(Objects.nonNull(region)){
            param.setLocationRegion(region);
        }

        InterruptStatus.Message response = stub.getInterruptStatusData(param.build());

        List<InterruptStatusVo> datalist = new ArrayList<>();
        List<InterruptStatus.data>  data = response.getDatalistList();
        for(InterruptStatus.data data1: data){
            InterruptStatusVo interruptStatusVo = new InterruptStatusVo();
            BeanUtils.copyProperties(data1, interruptStatusVo);
            List<InterruptStatus.data.interruptProject> interruptProjects = data1.getInterruptProjectsList();
            List<InterruptProjectVo> interruptProjectVoList = new ArrayList<>();
            for(InterruptStatus.data.interruptProject project: interruptProjects){
                InterruptProjectVo interruptProjectVo = new InterruptProjectVo();
                interruptProjectVo.setFloors(project.getFloors());
                com.google.protobuf.ProtocolStringList strings = project.getProjectList();

                int count = strings.size();
                String[] projects = new String[count];
                for(int i = 0; i < count; i++){
                    projects[i] = strings.get(i);
                }
                interruptProjectVo.setProject(projects);
                interruptProjectVoList.add(interruptProjectVo);
            }
            interruptStatusVo.setInterruptProjects(interruptProjectVoList);
            datalist.add(interruptStatusVo);
        }
        return  datalist;
    }




}
