package com.jillchen.grpc.client.vo;


import lombok.Data;

@Data
public class InterruptProjectVo {

    private String[] project;

    private String floors;
}
