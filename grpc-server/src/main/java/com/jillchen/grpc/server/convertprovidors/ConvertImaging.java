package com.jillchen.grpc.server.convertprovidors;


import com.aspose.imaging.FileFormat;
import com.aspose.imaging.Image;
import com.aspose.imaging.ImageOptionsBase;
import com.aspose.imaging.fileformats.psd.ColorModes;
import com.aspose.imaging.fileformats.psd.CompressionMethod;
import com.aspose.imaging.imageoptions.*;
import com.aspose.xps.rendering.BmpSaveOptions;
import com.jillchen.grpc.server.utils.AbsConverter;
import org.apache.commons.io.FilenameUtils;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.util.Arrays;

public class ConvertImaging extends AbsConverter {

    @Override
    public int Convert(String InFile, String OutFile) {
        try {
            long old = System.currentTimeMillis();
            Image image = Image.load(InFile);
            String FileExt = FilenameUtils.getExtension(OutFile).toUpperCase();
            if (FileExt.equals("PDF")) {
                com.aspose.imaging.imageoptions.PdfOptions psdOptions = new com.aspose.imaging.imageoptions.PdfOptions();
                // Save image to disk in PSD format
                image.save(OutFile, psdOptions);
            } else if (FileExt.equals("PSD")) {
                PsdOptions psdOptions = new PsdOptions();
                psdOptions.setColorMode(ColorModes.Rgb);
                psdOptions.setCompressionMethod(CompressionMethod.RLE);
                psdOptions.setVersion(4);
                image.save(OutFile, psdOptions);
            } else if (FileExt.equals("WEBP")) {
                // option
                ImageOptionsBase options = new WebPOptions();
                image.save(OutFile, options);
            } else {
                image.save(OutFile);
            }

            long now = System.currentTimeMillis();
            System.out.println("convert OK! " + ((now - old) / 1000.0) + "s");
            return 0;
        } catch (Exception ex) {
            System.out.println(ex.getMessage() + "\r\n" + Arrays.toString(ex.getStackTrace()));
            return -3;
        }
    }

    @Override
    public int Convert(InputStream inputStream, String InFile, ByteArrayOutputStream outputStream, String OutFile) {
        try {
            long old = System.currentTimeMillis();
            Image image = Image.load(inputStream);
            String FileExt = FilenameUtils.getExtension(OutFile).toUpperCase();
            if (FileExt.equals("PDF")) {
                com.aspose.imaging.imageoptions.PdfOptions psdOptions = new com.aspose.imaging.imageoptions.PdfOptions();
                // Save image to disk in PSD format
                image.save(outputStream, psdOptions);
            } else if (FileExt.equals("PSD")) {
                PsdOptions psdOptions = new PsdOptions();
                psdOptions.setColorMode(ColorModes.Rgb);
                psdOptions.setCompressionMethod(CompressionMethod.RLE);
                psdOptions.setVersion(4);
                image.save(outputStream, psdOptions);
            } else if (FileExt.equals("WEBP")) {
                // option
                ImageOptionsBase options = new WebPOptions();
                image.save(outputStream, options);
            } else {

                ImageOptionsBase options = new PngOptions();
                switch (FileExt) {
                    case "TIF":
                        options = new TiffOptions(0);
                        break;
                    case "JPEG":
                        options = new Jpeg2000Options();
                        break;
                    case "JPG":
                        options = new JpegOptions();
                        break;
                    case "PNG":
                        options = new PngOptions();
                        break;
                    case "TIFF":
                        options = new TiffOptions(0);
                        break;
                    case "GIF":
                        options = new GifOptions();
                        break;
                    case "SVG":
                        options = new SvgOptions();
                        break;
                    case "BMP":
                        options = new BmpOptions();
                        break;
                    default:
                        options = new PngOptions();
                        break;
                }
                // Initialize ImageSaveOptions

                image.save(outputStream, options);
            }

            long now = System.currentTimeMillis();
            System.out.println("convert OK! " + ((now - old) / 1000.0) + "s");
            return 0;
        } catch (Exception ex) {
            System.out.println(ex.getMessage() + "\r\n" + Arrays.toString(ex.getStackTrace()));
            return -3;
        }
    }
}
