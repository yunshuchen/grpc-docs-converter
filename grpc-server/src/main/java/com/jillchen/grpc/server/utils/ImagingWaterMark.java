package com.jillchen.grpc.server.utils;

import com.aspose.imaging.*;
import com.aspose.imaging.brushes.SolidBrush;

public class ImagingWaterMark {


       //添加文字水印
        public  static  void AddWatermark(Image image, String WaterMarkText)
        {

            //Create and initialize an instance of Graphics class.
            Graphics graphics = new Graphics(image);

            //Creates an instance of Font
            Font font = new Font("Times New Roman", 16, FontStyle.Bold);

            //Create an instance of SolidBrush and set its various properties.
            SolidBrush brush = new SolidBrush();
            brush.setColor(Color.getBlack());
            brush.setOpacity(100);

            //Draw a String using the SolidBrush object and Font, at specific Point.
            graphics.drawString(WaterMarkText, font, brush, new PointF(image.getWidth() - 100, image.getHeight() - 100));



        }

    /**
     * 添加倾斜水印
     * @param image
     * @param WaterMarkText
     */
    public  static  void AddDiagonalWatermark(Image image, String WaterMarkText)
        {



            // Create and initialize an instance of Graphics class and Initialize an object of SizeF to store image Size
            Graphics graphics = new Graphics(image);
            Size sz = graphics.getImage().getSize();

            // Creates an instance of Font, initialize it with Font Face, Size and Style
            Font font = new Font("Times New Roman", 20, FontStyle.Bold);

            // Create an instance of SolidBrush and set its various properties
            SolidBrush brush = new SolidBrush();
            brush.setColor(Color.getRed());
            brush.setOpacity(0);

            // Initialize an object of StringFormat class and set its various properties
            StringFormat format = new StringFormat();
            format.setAlignment(StringAlignment.Center);
            format.setFormatFlags(StringFormatFlags.MeasureTrailingSpaces);

            // Create an object of Matrix class for transformation
            Matrix matrix = new Matrix();

            // First a translation then a rotation
            matrix.translate(sz.getWidth() / 2f, sz.getHeight() / 2f);
            matrix.rotate(-45.0f);

            // Set the Transformation through Matrix
            graphics.setTransform(matrix);

            // Draw the string on Image Save output to disk
            graphics.drawString(WaterMarkText, font, brush, 0, 0, format);
        }
}
