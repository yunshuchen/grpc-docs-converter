package com.jillchen.grpc.server.convertprovidors;


import com.aspose.email.MailMessage;
import com.aspose.email.SaveOptions;
import com.aspose.words.Document;
import com.aspose.words.LoadFormat;
import com.aspose.words.LoadOptions;
import com.aspose.words.SaveFormat;
import com.jillchen.grpc.server.utils.AbsConverter;
import org.apache.commons.io.FilenameUtils;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Arrays;

public class ConvertEmail extends AbsConverter {
    @Override
    public int Convert(String InFile,String OutFile)  {

        try {
            long old = System.currentTimeMillis();


            FileInputStream fstream = new FileInputStream(InFile);
            MailMessage eml = MailMessage.load(fstream);

            // Save the Message to output stream in MHTML format
            ByteArrayOutputStream emlStream = new ByteArrayOutputStream();
            eml.save(emlStream, SaveOptions.getDefaultMhtml());

            // Load the stream in Word document
            LoadOptions lo = new LoadOptions();
            lo.setLoadFormat(LoadFormat.MHTML);

            Document doc = new Document(new ByteArrayInputStream(
                    emlStream.toByteArray()), lo);
            String FileExt = FilenameUtils.getExtension(OutFile).toUpperCase();
            // Save to disc
            doc.save(OutFile, SaveFormat.fromName(FileExt));
            long now = System.currentTimeMillis();
            System.out.println("convert OK! " + ((now - old) / 1000.0) + "s");
            return 0;
        }
        catch (Exception ex)
        {
            System.out.println(ex.getMessage()+"\r\n"+ Arrays.toString(ex.getStackTrace()));
            return  -3;
        }
    }

    @Override
    public int Convert(InputStream inputStream, String InFile, ByteArrayOutputStream outputStream, String OutFile) {

        try {
            long old = System.currentTimeMillis();



            MailMessage eml = MailMessage.load(inputStream);

            // Save the Message to output stream in MHTML format
            ByteArrayOutputStream emlStream = new ByteArrayOutputStream();
            eml.save(emlStream, SaveOptions.getDefaultMhtml());

            // Load the stream in Word document
            LoadOptions lo = new LoadOptions();
            lo.setLoadFormat(LoadFormat.MHTML);

            Document doc = new Document(new ByteArrayInputStream(
                    emlStream.toByteArray()), lo);
            String FileExt = FilenameUtils.getExtension(OutFile).toUpperCase();
            // Save to disc
            doc.save(outputStream, SaveFormat.fromName(FileExt));
            long now = System.currentTimeMillis();
            System.out.println("convert OK! " + ((now - old) / 1000.0) + "s");
            return 0;
        }
        catch (Exception ex)
        {
            System.out.println(ex.getMessage()+"\r\n"+ Arrays.toString(ex.getStackTrace()));
            return  -3;
        }
    }
}
