package com.jillchen.grpc.server.convertprovidors;




import com.aspose.tasks.HtmlSaveOptions;
import com.aspose.tasks.SaveFileFormat;
import com.jillchen.grpc.server.utils.AbsConverter;
import org.apache.commons.io.FilenameUtils;


import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.util.Arrays;

public class ConvertMpp  extends AbsConverter {

    @Override
    public int Convert(String InFile,String OutFile)  {

        try {

            long old = System.currentTimeMillis();

            com.aspose.tasks.Project doc = new com.aspose.tasks.Project(InFile);

            int f=SaveFileFormat.PDF;
            String FileExt = FilenameUtils.getExtension(OutFile).toUpperCase();
            for (Field e : SaveFileFormat.class.getDeclaredFields()) {
                if(e.getName().toUpperCase().equals(FileExt))
                {
                   f= e.getInt(SaveFileFormat.class);
                }
            }
            if("HTML".equals(FileExt))
            {

                //todo 处理html导出的问题
            }
            doc.save(OutFile, f);
            long now = System.currentTimeMillis();
            System.out.println("convert OK! " + ((now - old) / 1000.0) + "s");
            return 0;
        }
        catch (Exception ex)
        {
            System.out.println(ex.getMessage()+"\r\n"+ Arrays.toString(ex.getStackTrace()));
            return  -3;
        }
    }

    @Override
    public int Convert(InputStream inputStream, String InFile, ByteArrayOutputStream outputStream, String OutFile) {

        try {

            long old = System.currentTimeMillis();

            com.aspose.tasks.Project doc = new com.aspose.tasks.Project(inputStream);

            int f=SaveFileFormat.PDF;
            String FileExt = FilenameUtils.getExtension(OutFile).toUpperCase();
            for (Field e : SaveFileFormat.class.getDeclaredFields()) {
                if(e.getName().toUpperCase().equals(FileExt))
                {
                    f= e.getInt(SaveFileFormat.class);
                }
            }
            if("HTML".equals(FileExt))
            {

                //todo 处理html导出的问题
            }
            doc.save(outputStream, f);
            long now = System.currentTimeMillis();
            System.out.println("convert OK! " + ((now - old) / 1000.0) + "s");
            return 0;
        }
        catch (Exception ex)
        {
            System.out.println(ex.getMessage()+"\r\n"+ Arrays.toString(ex.getStackTrace()));
            return  -3;
        }
    }
}
