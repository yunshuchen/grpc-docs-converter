package com.jillchen.grpc.server;

import com.aspose.html.rendering.image.ImageFormat;
import com.aspose.pdf.*;


import com.aspose.pdf.facades.PdfFileEditor;
import com.aspose.pdf.text.FontSourceCollection;
import com.aspose.pdf.text.SimpleFontSubstitution;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

@SpringBootApplication
public class GrpcServerApplication {

    public static void main(String[] args) throws FileNotFoundException {
        SpringApplication.run(com.jillchen.grpc.server.GrpcServerApplication.class, args);
    }

}
