package com.jillchen.grpc.server.utils;

import com.aspose.slides.exceptions.Exception;

public class WordWaterMark {


    public  static    void insertWatermarkText(com.aspose.words.Document doc, String watermarkText) throws java.lang.Exception {
        // Create a watermark shape. This will be a WordArt shape.
        // You are free to try other shape types as watermarks.
        com.aspose.words.Shape watermark = new com.aspose.words.Shape(doc, com.aspose.words.ShapeType.TEXT_PLAIN_TEXT);

        // Set up the text of the watermark.
        watermark.getTextPath().setText(watermarkText);
        watermark.getTextPath().setFontFamily("Arial");
        watermark.setWidth(500);
        watermark.setHeight(100);
        // Text will be directed from the bottom-left to the top-right corner.
        watermark.setRotation(-40);
        // Remove the following two lines if you need a solid black text.
        watermark.getFill().setColor(java.awt.Color.GRAY); // Try LightGray to get more Word-style watermark
        watermark.setStrokeColor(java.awt.Color.GRAY); // Try LightGray to get more Word-style watermark

        // Place the watermark in the page center.
        watermark.setRelativeHorizontalPosition(com.aspose.words.RelativeHorizontalPosition.PAGE);
        watermark.setRelativeVerticalPosition(com.aspose.words.RelativeVerticalPosition.PAGE);
        watermark.setWrapType(com.aspose.words.WrapType.NONE);
        watermark.setVerticalAlignment(com.aspose.words.VerticalAlignment.CENTER);
        watermark.setHorizontalAlignment(com.aspose.words.HorizontalAlignment.CENTER);

        // Create a new paragraph and append the watermark to this paragraph.
        com.aspose.words.Paragraph watermarkPara = new com.aspose.words.Paragraph(doc);
        watermarkPara.appendChild(watermark);

        // Insert the watermark into all headers of each document section.
        for (com.aspose.words.Section sect : doc.getSections())
        {
            // There could be up to three different headers in each section, since we want
            // the watermark to appear on all pages, insert into all headers.
            insertWatermarkIntoHeader(watermarkPara, sect, com.aspose.words.HeaderFooterType.HEADER_PRIMARY);
            insertWatermarkIntoHeader(watermarkPara, sect, com.aspose.words.HeaderFooterType.HEADER_FIRST);
            insertWatermarkIntoHeader(watermarkPara, sect, com.aspose.words.HeaderFooterType.HEADER_EVEN);
        }
        System.out.println("Done.");
    }

    private  static void insertWatermarkIntoHeader(com.aspose.words.Paragraph watermarkPara, com.aspose.words.Section sect, int headerType) throws Exception
    {
        com.aspose.words.HeaderFooter header = sect.getHeadersFooters().getByHeaderFooterType(headerType);

        if (header == null)
        {
            // There is no header of the specified type in the current section, create it.
            header = new com.aspose.words.HeaderFooter(sect.getDocument(), headerType);
            sect.getHeadersFooters().add(header);
        }

        // Insert a clone of the watermark into the header.
        header.appendChild(watermarkPara.deepClone(true));
    }
}
