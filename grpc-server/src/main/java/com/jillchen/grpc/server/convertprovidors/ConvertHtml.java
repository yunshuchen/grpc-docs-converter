package com.jillchen.grpc.server.convertprovidors;




import com.aspose.html.internal.ms.System.IO.Path;
import com.aspose.html.rendering.image.ImageFormat;
import com.jillchen.grpc.server.utils.AbsConverter;
import com.jillchen.grpc.server.utils.DomainUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;


import java.io.*;
import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.UUID;

public class ConvertHtml extends AbsConverter {
    @Override
    public int Convert(String InFile,String OutFile) {
        try {

            long old = System.currentTimeMillis();

            String InFileExt = FilenameUtils.getExtension(InFile).toUpperCase();
            String FileExt = FilenameUtils.getExtension(OutFile).toUpperCase();
            final boolean b1 = "JPG".equals(FileExt) ||"JPEG".equals(FileExt) || "PNG".equals(FileExt) || "BMP".equals(FileExt) || "GIF".equals(FileExt) || "TIFF".equals(FileExt);
            if("HTML".equals(InFileExt)||"HTM".equals(InFileExt)) {
                com.aspose.html.HTMLDocument htmlDocument = new com.aspose.html.HTMLDocument(InFile);
                if (b1) {
                    int f = ImageFormat.Png;
                    for (Field e : ImageFormat.class.getDeclaredFields()) {
                        if (e.getName().toUpperCase().equals(FileExt)) {
                            f = e.getInt(ImageFormat.class);
                        }
                    }
                    // Initialize ImageSaveOptions
                    com.aspose.html.saving.ImageSaveOptions options = new com.aspose.html.saving.ImageSaveOptions(f);
                    // Convert HTML to JPEG
                    com.aspose.html.converters.Converter.convertHTML(htmlDocument, options, OutFile);

                }  else if ("XPS".equals(FileExt)) {
                    // Initialize XpsSaveOptions
                    com.aspose.html.saving.XpsSaveOptions options = new com.aspose.html.saving.XpsSaveOptions();
                    options.setBackgroundColor(com.aspose.html.drawing.Color.getCyan());
                    // Convert HTML to XPS
                    com.aspose.html.converters.Converter.convertHTML(htmlDocument, options, OutFile);
                } else if ("MD".equals(FileExt)) {
                    htmlDocument.save(OutFile, com.aspose.html.saving.HTMLSaveFormat.Markdown);
                }else {
                    // Source HTML document

                    // Initialize PdfSaveOptions
                    com.aspose.html.saving.PdfSaveOptions options = new com.aspose.html.saving.PdfSaveOptions();
                    options.setJpegQuality(100);
                    // Convert HTML to PDF
                    com.aspose.html.converters.Converter.convertHTML(htmlDocument, options, OutFile);
                }
            }
            else if("EPUB".equals(InFileExt)||"MHT".equals(InFileExt))
            {

                FileInputStream fileInputStream = new FileInputStream(InFile);
                if (b1) {

                    int f = ImageFormat.Png;
                    for (Field e : ImageFormat.class.getDeclaredFields()) {
                        if (e.getName().toUpperCase().equals(FileExt)) {
                            f = e.getInt(ImageFormat.class);
                        }
                    }
                    // Initialize ImageSaveOptions
                    com.aspose.html.saving.ImageSaveOptions options = new com.aspose.html.saving.ImageSaveOptions(f);
                    if("EPUB".equals(InFileExt)){
                        com.aspose.html.converters.Converter.convertEPUB(fileInputStream, options, OutFile);
                    }else
                    {
                        com.aspose.html.converters.Converter.convertMHTML(fileInputStream, options, OutFile);
                    }

                }else if ("XPS".equals(FileExt)) {
                    // Initialize XpsSaveOptions
                    com.aspose.html.saving.XpsSaveOptions options = new com.aspose.html.saving.XpsSaveOptions();
                    options.setBackgroundColor(com.aspose.html.drawing.Color.getCyan());
                    if("EPUB".equals(InFileExt)){
                        com.aspose.html.converters.Converter.convertEPUB(fileInputStream, options, OutFile);
                    }else {
                        com.aspose.html.converters.Converter.convertMHTML(fileInputStream, options, OutFile);
                    }
                } else {
                    // Source HTML document

                    // Initialize PdfSaveOptions
                    com.aspose.html.saving.PdfSaveOptions options = new com.aspose.html.saving.PdfSaveOptions();
                    options.setJpegQuality(100);
                    // Convert HTML to PDF
                    if("EPUB".equals(InFileExt)){
                        com.aspose.html.converters.Converter.convertEPUB(fileInputStream, options, OutFile);
                    }else {
                        com.aspose.html.converters.Converter.convertMHTML(fileInputStream, options, OutFile);
                    }
                }

            }
            else if("MD".equals(InFileExt))
            {
                if ("PDF".equals(FileExt)) {
                    com.aspose.html.converters.Converter.convertMarkdown(
                            InFile,OutFile
                    );
                }else if ("HTML".equals(FileExt)) {
                com.aspose.html.converters.Converter.convertMarkdown(
                        InFile,OutFile
                );
            }
            }
            long now = System.currentTimeMillis();
            System.out.println("convert OK! " + ((now - old) / 1000.0) + "s");
            return 0;
        }
        catch (Exception ex)
        {
            System.out.println(ex.getMessage()+"\r\n"+ Arrays.toString(ex.getStackTrace()));
            return  -3;
        }
    }


    @Override
    public int Convert(InputStream inputStream, String InFile1, ByteArrayOutputStream outputStream, String OutFile1) {
        try {

            long old = System.currentTimeMillis();

            String InFileExt = FilenameUtils.getExtension(InFile1).toUpperCase();
            String FileExt = FilenameUtils.getExtension(OutFile1).toUpperCase();

            String TempPath=DomainUtils.Instance.GetTempPath();
            String FileName=UUID.randomUUID()+"."+FileExt;
            OutFile1=Path.combine(TempPath,FileName);

            final boolean b1 = "JPG".equals(FileExt) ||"JPEG".equals(FileExt) || "PNG".equals(FileExt) || "BMP".equals(FileExt) || "GIF".equals(FileExt) || "TIFF".equals(FileExt);
            if("HTML".equals(InFileExt)||"HTM".equals(InFileExt)) {
                com.aspose.html.HTMLDocument htmlDocument = new com.aspose.html.HTMLDocument(inputStream,"");
                if (b1) {
                    int f = ImageFormat.Png;
                    for (Field e : ImageFormat.class.getDeclaredFields()) {
                        if (e.getName().toUpperCase().equals(FileExt)) {
                            f = e.getInt(ImageFormat.class);
                        }
                    }
                    // Initialize ImageSaveOptions
                    com.aspose.html.saving.ImageSaveOptions options = new com.aspose.html.saving.ImageSaveOptions(f);
                    // Convert HTML to JPEG
                    com.aspose.html.converters.Converter.convertHTML(htmlDocument, options, OutFile1);
                }  else if ("XPS".equals(FileExt)) {
                    // Initialize XpsSaveOptions
                    com.aspose.html.saving.XpsSaveOptions options = new com.aspose.html.saving.XpsSaveOptions();
                    options.setBackgroundColor(com.aspose.html.drawing.Color.getCyan());
                    // Convert HTML to JPEG
                    com.aspose.html.converters.Converter.convertHTML(htmlDocument, options, OutFile1);
                } else if ("MD".equals(FileExt)) {
                    // Convert HTML to JPEG
                    htmlDocument.save( OutFile1, com.aspose.html.saving.HTMLSaveFormat.Markdown);
                }else {
                    // Source HTML document
                    // Initialize PdfSaveOptions
                    com.aspose.html.saving.PdfSaveOptions options = new com.aspose.html.saving.PdfSaveOptions();
                    options.setJpegQuality(100);
                    com.aspose.html.converters.Converter.convertHTML(htmlDocument, options, OutFile1);
                }
                java.io.OutputStream text_stream = FileUtils.openOutputStream(new File(OutFile1));
                outputStream= (ByteArrayOutputStream)text_stream;
                text_stream.close();
            }
            else if("EPUB".equals(InFileExt)||"MHT".equals(InFileExt))
            {

                if (b1) {

                    int f = ImageFormat.Png;
                    for (Field e : ImageFormat.class.getDeclaredFields()) {
                        if (e.getName().toUpperCase().equals(FileExt)) {
                            f = e.getInt(ImageFormat.class);
                        }
                    }
                    // Initialize ImageSaveOptions
                    com.aspose.html.saving.ImageSaveOptions options = new com.aspose.html.saving.ImageSaveOptions(f);
                    if("EPUB".equals(InFileExt)){
                        // Convert HTML to JPEG
                        com.aspose.html.converters.Converter.convertEPUB(inputStream, options, OutFile1);
                    }else
                    {

                        // Convert HTML to JPEG
                        com.aspose.html.converters.Converter.convertMHTML(inputStream, options, OutFile1);
                    }
                    java.io.OutputStream text_stream = FileUtils.openOutputStream(new File(OutFile1));
                    outputStream= (ByteArrayOutputStream)text_stream;
                    text_stream.close();
                }else if ("XPS".equals(FileExt)) {
                    // Initialize XpsSaveOptions
                    com.aspose.html.saving.XpsSaveOptions options = new com.aspose.html.saving.XpsSaveOptions();
                    options.setBackgroundColor(com.aspose.html.drawing.Color.getCyan());
                    if("EPUB".equals(InFileExt)){
                        com.aspose.html.converters.Converter.convertEPUB(inputStream, options, OutFile1);

                    }else {
                        com.aspose.html.converters.Converter.convertMHTML(inputStream, options, OutFile1);

                    }
                    java.io.OutputStream text_stream = FileUtils.openOutputStream(new File(OutFile1));
                    outputStream= (ByteArrayOutputStream)text_stream;
                    text_stream.close();
                } else {
                    // Source HTML document

                    // Initialize PdfSaveOptions
                    com.aspose.html.saving.PdfSaveOptions options = new com.aspose.html.saving.PdfSaveOptions();
                    options.setJpegQuality(100);
                    // Convert HTML to PDF
                    if("EPUB".equals(InFileExt)){
                        com.aspose.html.converters.Converter.convertEPUB(inputStream, options, OutFile1);
                    }else {
                        com.aspose.html.converters.Converter.convertMHTML(inputStream, options, OutFile1);
                    }
                    java.io.OutputStream text_stream = FileUtils.openOutputStream(new File(OutFile1));
                    outputStream= (ByteArrayOutputStream)text_stream;
                    text_stream.close();
                }

            }
            else if("MD".equals(InFileExt))
            {
               if ("HTML".equals(FileExt)) {
                    com.aspose.html.converters.Converter.convertMarkdown(
                            inputStream,OutFile1
                    );
                }else
               {
                   com.aspose.html.converters.Converter.convertMarkdown(
                           inputStream,OutFile1
                   );
               }
                java.io.OutputStream text_stream = FileUtils.openOutputStream(new File(OutFile1));
                outputStream= (ByteArrayOutputStream)text_stream;
                text_stream.close();
            }
            long now = System.currentTimeMillis();
            System.out.println("convert OK! " + ((now - old) / 1000.0) + "s");
            return 0;
        }
        catch (Exception ex)
        {
            System.out.println(ex.getMessage()+"\r\n"+ Arrays.toString(ex.getStackTrace()));
            return  -3;
        }
    }
}
