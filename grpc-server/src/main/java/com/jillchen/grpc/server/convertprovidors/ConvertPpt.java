package com.jillchen.grpc.server.convertprovidors;



import com.aspose.slides.SaveFormat;
import com.jillchen.grpc.server.utils.AbsConverter;
import org.apache.commons.io.FilenameUtils;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.util.Arrays;

public class ConvertPpt extends AbsConverter {
    @Override
    public int Convert(String InFile,String OutFile) {
        try {

            long old = System.currentTimeMillis();

            com.aspose.slides.Presentation doc = new com.aspose.slides.Presentation(InFile);
            int f= SaveFormat.Pdf;
            String FileExt = FilenameUtils.getExtension(OutFile).toUpperCase();
            for (Field e : SaveFormat.class.getDeclaredFields()) {
                if(e.getName().toUpperCase().equals(FileExt))
                {
                    f= e.getInt(SaveFormat.class);
                }
            }
            doc.save(OutFile,f);
            long now = System.currentTimeMillis();
            System.out.println("convert OK! " + ((now - old) / 1000.0) + "s");
            return 0;
        }
        catch (Exception ex)
        {
            System.out.println(ex.getMessage()+"\r\n"+ Arrays.toString(ex.getStackTrace()));
            return  -3;
        }
    }

    @Override
    public int Convert(InputStream inputStream, String InFile, ByteArrayOutputStream outputStream, String OutFile) {
        try {
            long old = System.currentTimeMillis();
            com.aspose.slides.Presentation doc = new com.aspose.slides.Presentation(inputStream);
            int f= SaveFormat.Pdf;
            String FileExt = FilenameUtils.getExtension(OutFile).toUpperCase();
            for (Field e : SaveFormat.class.getDeclaredFields()) {
                if(e.getName().toUpperCase().equals(FileExt))
                {
                    f= e.getInt(SaveFormat.class);
                }
            }
            doc.save(outputStream,f);
            long now = System.currentTimeMillis();
            System.out.println("convert OK! " + ((now - old) / 1000.0) + "s");
            return 0;
        }
        catch (Exception ex)
        {
            System.out.println(ex.getMessage()+"\r\n"+ Arrays.toString(ex.getStackTrace()));
            return  -3;
        }
    }
}
