package com.jillchen.grpc.server.convertprovidors;



import com.aspose.eps.PsDocument;
import com.aspose.eps.device.PdfSaveOptions;
import com.aspose.xps.LoadOptions;
import com.aspose.xps.XpsDocument;
import com.aspose.xps.XpsLoadOptions;
import com.jillchen.grpc.server.utils.AbsConverter;
import org.apache.commons.io.FilenameUtils;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.util.Arrays;

public class ConvertPage extends AbsConverter {
    @Override
    public int Convert(String InFile,String OutFile) {
        try {

            long old = System.currentTimeMillis();
            String FileExt = FilenameUtils.getExtension(InFile).toUpperCase();
            String OutFileExt = FilenameUtils.getExtension(OutFile).toUpperCase();

            if(FileExt.equals("PS")) {
                // Initialize PostScript input stream
                FileInputStream psStream = new FileInputStream(InFile);
                PsDocument document = new PsDocument(psStream);
                //Initialize options object with necessary parameters.
                PdfSaveOptions options = new PdfSaveOptions(true);
                // If you want to add special folder where fonts are stored. Default fonts folder in OS is always included.
                //options.setAdditionalFontsFolders(new String [] {"FONTS_FOLDER"});

                FileOutputStream pdfStream = new FileOutputStream(OutFile);

                if(OutFileExt.equals("PDF")) {                // Default page size is 595x842 and it is not mandatory to set it in PdfDevice
                    com.aspose.eps.device.PdfDevice device = new com.aspose.eps.device.PdfDevice(pdfStream);
                    // But if you need to specify size and image format use following line
                    //com.aspose.eps.device.PdfDevice device = new com.aspose.eps.device.PdfDevice(pdfStream, new Dimension(595, 842));
                    document.save(device, options);
                }else
                {
                    // Create rendering device for PDF format
                    com.aspose.xps.rendering.ImageDevice device = new com.aspose.xps.rendering.ImageDevice();

                    document.save(device, options);
                    // Iterate through document partitions (fixed documents, in XPS terms)
                    for (int i = 0; i < device.getResult().length; i++) {
                        // Iterate through partition pages
                        for (int j = 0; j < device.getResult()[i].length; j++) {
                            // Initialize image output stream
                            FileOutputStream imageStream = new FileOutputStream("./temp/" + "PStoBMP" + "_" + (i + 1) + "_" + (j + 1) + ".png");
                            // Write image
                            imageStream.write(device.getResult()[i][j], 0, device.getResult()[i][j].length);
                            imageStream.close();
                        }
                    }

                }

            }else{
                // Initialize PDF output stream
                FileOutputStream pdfStream = new FileOutputStream(OutFile);

                // Load XPS document
                XpsDocument document = new XpsDocument(InFile);

                // Initialize options object with necessary parameters.
                com.aspose.xps.rendering.PdfSaveOptions options = new com.aspose.xps.rendering.PdfSaveOptions();
                options.setJpegQualityLevel(100);
                options.setImageCompression(com.aspose.xps.rendering.PdfImageCompression.Jpeg);
                options.setTextCompression(com.aspose.xps.rendering.PdfTextCompression.Flate);

                if(OutFileExt.equals("PDF")) {

                    // Create rendering device for PDF format
                    com.aspose.xps.rendering.PdfDevice device = new com.aspose.xps.rendering.PdfDevice(pdfStream);

                    document.save(device, options);
                }
                else
                {
                    // Create rendering device for PDF format
                    com.aspose.xps.rendering.ImageDevice device = new com.aspose.xps.rendering.ImageDevice();
                    document.save(device, options);
                    // Iterate through document partitions (fixed documents, in XPS terms)
                    for (int i = 0; i < device.getResult().length; i++) {
                        // Iterate through partition pages
                        for (int j = 0; j < device.getResult()[i].length; j++) {
                            // Initialize image output stream
                            FileOutputStream imageStream = new FileOutputStream("./temp/" + "XPStoBMP" + "_" + (i + 1) + "_" + (j + 1) + ".png");
                            // Write image
                            imageStream.write(device.getResult()[i][j], 0, device.getResult()[i][j].length);
                            imageStream.close();
                        }
                    }
                }
            }
            long now = System.currentTimeMillis();
            System.out.println("convert OK! " + ((now - old) / 1000.0) + "s");
            return 0;
        }
        catch (Exception ex)
        {
            System.out.println(ex.getMessage()+"\r\n"+ Arrays.toString(ex.getStackTrace()));
            return  -3;
        }
    }


    @Override
    public int Convert(InputStream inputStream, String InFile, ByteArrayOutputStream outputStream, String OutFile) {
        try {

            long old = System.currentTimeMillis();
            String FileExt = FilenameUtils.getExtension(InFile).toUpperCase();
            String OutFileExt = FilenameUtils.getExtension(OutFile).toUpperCase();

            if(FileExt.equals("PS")) {
                // Initialize PostScript input stream

                PsDocument document = new PsDocument(inputStream);
                //Initialize options object with necessary parameters.
                PdfSaveOptions options = new PdfSaveOptions(true);
                // If you want to add special folder where fonts are stored. Default fonts folder in OS is always included.
                //options.setAdditionalFontsFolders(new String [] {"FONTS_FOLDER"});
                if(OutFileExt.equals("PDF")) {                // Default page size is 595x842 and it is not mandatory to set it in PdfDevice
                    com.aspose.eps.device.PdfDevice device = new com.aspose.eps.device.PdfDevice(outputStream);
                    // But if you need to specify size and image format use following line
                    //com.aspose.eps.device.PdfDevice device = new com.aspose.eps.device.PdfDevice(pdfStream, new Dimension(595, 842));
                    document.save(device, options);
                }else
                {
                    // Create rendering device for PDF format
                    com.aspose.xps.rendering.ImageDevice device = new com.aspose.xps.rendering.ImageDevice();

                    document.save(device, options);
                    // Iterate through document partitions (fixed documents, in XPS terms)
                    for (int i = 0; i < device.getResult().length; i++) {
                        // Iterate through partition pages
                        for (int j = 0; j < device.getResult()[i].length; j++) {
                            // Initialize image output stream
                            FileOutputStream imageStream = new FileOutputStream("./temp/" + "PStoBMP" + "_" + (i + 1) + "_" + (j + 1) + ".png");
                            // Write image
                            imageStream.write(device.getResult()[i][j], 0, device.getResult()[i][j].length);
                            imageStream.close();
                        }
                    }

                }

            }else{
                // Initialize PDF output stream
                FileOutputStream pdfStream = new FileOutputStream(OutFile);

                XpsLoadOptions xpsLoadOptions=new XpsLoadOptions();

                // Load XPS document
                XpsDocument document = new XpsDocument(inputStream ,xpsLoadOptions);

                // Initialize options object with necessary parameters.
                com.aspose.xps.rendering.PdfSaveOptions options = new com.aspose.xps.rendering.PdfSaveOptions();
                options.setJpegQualityLevel(100);
                options.setImageCompression(com.aspose.xps.rendering.PdfImageCompression.Jpeg);
                options.setTextCompression(com.aspose.xps.rendering.PdfTextCompression.Flate);

                if(OutFileExt.equals("PDF")) {

                    // Create rendering device for PDF format
                    com.aspose.xps.rendering.PdfDevice device = new com.aspose.xps.rendering.PdfDevice(outputStream);

                    document.save(device, options);
                }
                else
                {
                    // Create rendering device for PDF format
                    com.aspose.xps.rendering.ImageDevice device = new com.aspose.xps.rendering.ImageDevice();
                    document.save(device, options);
                    // Iterate through document partitions (fixed documents, in XPS terms)
                    for (int i = 0; i < device.getResult().length; i++) {
                        // Iterate through partition pages
                        for (int j = 0; j < device.getResult()[i].length; j++) {
                            // Initialize image output stream
                            FileOutputStream imageStream = new FileOutputStream("./temp/" + "XPStoBMP" + "_" + (i + 1) + "_" + (j + 1) + ".png");
                            // Write image
                            imageStream.write(device.getResult()[i][j], 0, device.getResult()[i][j].length);
                            imageStream.close();
                        }
                    }
                }
            }
            long now = System.currentTimeMillis();
            System.out.println("convert OK! " + ((now - old) / 1000.0) + "s");
            return 0;
        }
        catch (Exception ex)
        {
            System.out.println(ex.getMessage()+"\r\n"+ Arrays.toString(ex.getStackTrace()));
            return  -3;
        }
    }
}
