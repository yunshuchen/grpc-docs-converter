package com.jillchen.grpc.server.convertprovidors;

import com.aspose.pub.Document;
import com.aspose.pub.IPubParser;
import com.aspose.pub.PubFactory;
import com.jillchen.grpc.server.utils.AbsConverter;


import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.Arrays;

public class ConvertPub extends AbsConverter {

    @Override
    public int Convert(String InFile,String OutFile)  {

        try {

            long old = System.currentTimeMillis();
            IPubParser parser = PubFactory.createParser(InFile);

            Document doc = parser.parse();

            PubFactory.createPdfConverter().convertToPdf(doc, OutFile);
            long now = System.currentTimeMillis();
            System.out.println("convert OK! " + ((now - old) / 1000.0) + "s");
            return 0;
        }
        catch (Exception ex)
        {
            System.out.println(ex.getMessage()+"\r\n"+ Arrays.toString(ex.getStackTrace()));
            return  -3;
        }
    }
    @Override
    public int Convert(InputStream inputStream, String InFile, ByteArrayOutputStream outputStream, String OutFile) {

        try {

            long old = System.currentTimeMillis();
            IPubParser parser = PubFactory.createParser(inputStream);
            Document doc = parser.parse();
            PubFactory.createPdfConverter().convertToPdf(doc, outputStream);
            long now = System.currentTimeMillis();
            System.out.println("convert OK! " + ((now - old) / 1000.0) + "s");
            return 0;
        }
        catch (Exception ex)
        {
            System.out.println(ex.getMessage()+"\r\n"+ Arrays.toString(ex.getStackTrace()));
            return  -3;
        }
    }
}
