package com.jillchen.grpc.server.utils;

import com.google.protobuf.ByteString;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

public class TypeConverter {
    /**
     * byte[]转String
     *
     * @param bytes
     * @return
     * @throws Exception
     */
    public static String bytesToString(byte[] bytes) throws Exception {
        String result = new String(bytes, "UTF-8");
        return result;
    }

    /**
     * String转byte[]
     *
     * @param str
     * @return
     */
    public static byte[] StringToBytes(String str) {
        byte[] bytes = str.getBytes();
        return bytes;
    }


    /**
     * InputStream转String
     *
     * @param inputStream
     * @return
     * @throws Exception
     */
    public static String InputStreamToString(InputStream inputStream) throws Exception {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        int bufferSize = 1024;
        byte[] data = new byte[bufferSize];
        int count = -1;
        while ((count = inputStream.read(data, 0, bufferSize)) != -1) {
            outputStream.write(data, 0, count);
            data = null;
        }
        return new String(outputStream.toByteArray(), "UTF-8");
    }

    /**
     * InputStream转Byte[]
     *
     * @param inputStream
     * @return
     * @throws Exception
     */
    public static byte[] InputStreamToBytes(InputStream inputStream) throws Exception {
        int bufferSize = 1024;
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        byte[] data = new byte[bufferSize];
        int count = -1;
        while ((count = inputStream.read(data, 0, bufferSize)) != -1) {
            outputStream.write(data, 0, count);
            data = null;
        }

        return outputStream.toByteArray();
    }

    /**
     * byte[]转InputStream
     *
     * @param bytes
     * @return
     * @throws Exception
     */
    public static InputStream BytesToInputStream(byte[] bytes) throws Exception {
        ByteArrayInputStream is = new ByteArrayInputStream(bytes);
        return is;
    }

    public static ByteArrayOutputStream InputStream2ByteArrayOutputStream(InputStream input) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        byte[] buffer = new byte[1024];
        int len;
        try {
            while ((len = input.read(buffer)) > -1) {
                baos.write(buffer, 0, len);
            }
            baos.flush();
        } catch (IOException e) {

        } finally {
            try {
                input.close();
            } catch (IOException e) {
            }
        }
        return baos;
    }

    public static InputStream ByteArrayOutputStream2InputStream(ByteArrayOutputStream in) {
        return new ByteArrayInputStream(in.toByteArray());
    }

    public static byte[] ByteArrayOutputStream2Byte(ByteArrayOutputStream stream) {
        return stream.toByteArray();
    }


    public static ByteString Byte2ByteString(byte[] stream) {
        return ByteString.copyFrom(stream);
    }
}
