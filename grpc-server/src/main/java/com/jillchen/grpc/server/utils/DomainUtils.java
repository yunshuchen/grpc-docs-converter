package com.jillchen.grpc.server.utils;

import com.aspose.html.internal.ms.System.IO.Path;
import javafx.application.Application;
import org.springframework.boot.system.ApplicationHome;

import java.io.File;

public  class DomainUtils {

    public  static DomainUtils Instance=new DomainUtils();
    public  String GetRootPath() {
        //第五种
        ApplicationHome h = new ApplicationHome(getClass());
        File jarF = h.getSource();
        return jarF.getParentFile().toString();
    }

    public  String GetTempPath()
    {
        String Temp=Path.combine(GetRootPath(),"temp");
        File file1 = new File(Temp);
        if(!file1 .exists()) {
            file1.mkdirs();//创建目录
        }
        return  Temp;
    }
}
