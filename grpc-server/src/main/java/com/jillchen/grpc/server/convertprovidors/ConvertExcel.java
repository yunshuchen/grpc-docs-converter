package com.jillchen.grpc.server.convertprovidors;

import com.aspose.cells.*;
import com.jillchen.grpc.server.utils.AbsConverter;
import com.jillchen.grpc.server.utils.TypeConverter;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Arrays;

public class ConvertExcel extends AbsConverter {

    @Override
    public int Convert(String InFile,String OutFile)  {

        try {
            long old = System.currentTimeMillis();
            Workbook  doc = new Workbook(InFile);

            String FileExt = FilenameUtils.getExtension(OutFile).toUpperCase();
            SaveOptions xlsSaveOption= new  PdfSaveOptions();
            if(FileExt.equals("PDF")) {

                xlsSaveOption = new PdfSaveOptions();
                PdfSecurityOptions securityOptions=  new  PdfSecurityOptions();
                securityOptions.setExtractContentPermission(false);
                securityOptions.setPrintPermission(false);
                ((PdfSaveOptions) xlsSaveOption).setSecurityOptions(securityOptions);
                ((PdfSaveOptions) xlsSaveOption).setAllColumnsInOnePagePerSheet(true);
                doc.save(OutFile, xlsSaveOption);
            }
            else if(FileExt.equals("JSON")) {

                // Load CSV file
                Workbook workbook = new Workbook(InFile);
                Cell lastCell = workbook.getWorksheets().get(0).getCells().getLastCell();

                // Set ExportRangeToJsonOptions
                ExportRangeToJsonOptions options = new ExportRangeToJsonOptions();
                Range range = workbook.getWorksheets().get(0).getCells().createRange(0, 0, lastCell.getRow() + 1, lastCell.getColumn() + 1);
                String data = JsonUtility.exportRangeToJson(range, options);
                FileUtils.writeStringToFile(new File(OutFile),data);
            }
            else
            {
                doc.save(OutFile);
            }
            long now = System.currentTimeMillis();
            System.out.println("convert OK! " + ((now - old) / 1000.0) + "s");
            return 0;
        }
        catch (Exception ex)
        {
            System.out.println(ex.getMessage()+"\r\n"+ Arrays.toString(ex.getStackTrace()));
            return  -3;
        }
    }

    @Override
    public int Convert(InputStream inputStream, String InFile, ByteArrayOutputStream outputStream, String OutFile) {

        try {
            long old = System.currentTimeMillis();
            Workbook  doc = new Workbook(inputStream);

            String FileExt = FilenameUtils.getExtension(OutFile).toUpperCase();
            SaveOptions xlsSaveOption= new  PdfSaveOptions();
            if(FileExt.equals("PDF")) {

                xlsSaveOption = new PdfSaveOptions();
                PdfSecurityOptions securityOptions=  new  PdfSecurityOptions();
                securityOptions.setExtractContentPermission(false);
                securityOptions.setPrintPermission(false);
                ((PdfSaveOptions) xlsSaveOption).setSecurityOptions(securityOptions);
                ((PdfSaveOptions) xlsSaveOption).setAllColumnsInOnePagePerSheet(true);
                doc.save(outputStream, xlsSaveOption);
            }
            else if(FileExt.equals("JSON")) {
                Cell lastCell = doc.getWorksheets().get(0).getCells().getLastCell();
                // Set ExportRangeToJsonOptions
                ExportRangeToJsonOptions options = new ExportRangeToJsonOptions();
                Range range = doc.getWorksheets().get(0).getCells().createRange(0, 0, lastCell.getRow() + 1, lastCell.getColumn() + 1);
                String data = JsonUtility.exportRangeToJson(range, options);
                byte[] bys= data.getBytes();
                outputStream=new ByteArrayOutputStream(bys.length);

            }
            else
            {
                FileFormatInfo ffi=FileFormatUtil.detectFileFormat(OutFile);
                doc.save(outputStream,ffi.getFileFormatType());
            }
            long now = System.currentTimeMillis();
            System.out.println("convert OK! " + ((now - old) / 1000.0) + "s");
            return 0;
        }
        catch (Exception ex)
        {
            System.out.println(ex.getMessage()+"\r\n"+ Arrays.toString(ex.getStackTrace()));
            return  -3;
        }
    }
}
