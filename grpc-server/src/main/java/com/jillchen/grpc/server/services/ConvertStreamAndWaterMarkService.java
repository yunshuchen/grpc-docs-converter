package com.jillchen.grpc.server.services;


import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.google.protobuf.ByteString;
import com.jillchen.grpc.libs.DocsStreamConvertGrpc;
import com.jillchen.grpc.libs.ConvertStream;
import com.jillchen.grpc.server.convertprovidors.ConvertPdf;
import com.jillchen.grpc.server.entitys.ExtType;
import com.jillchen.grpc.server.utils.AbsConverter;
import com.jillchen.grpc.server.utils.NonEmptyInputStream;
import com.jillchen.grpc.server.utils.TypeConverter;
import io.grpc.stub.StreamObserver;
import lombok.extern.slf4j.Slf4j;
import net.devh.springboot.autoconfigure.grpc.server.GrpcService;
import org.apache.commons.io.FilenameUtils;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@GrpcService(ConvertStream.class)
public class ConvertStreamAndWaterMarkService extends DocsStreamConvertGrpc.DocsStreamConvertImplBase {
    @Override
    public void convert(ConvertStream.RequestInputFileStream request, StreamObserver<ConvertStream.ResponseStreamReply> responseObserver){

        //输入文件流
        ByteString bys = request.getInputStream();
        byte[] btyeArr = bys.toByteArray();
        InputStream inputStream = new ByteArrayInputStream(btyeArr);
        //输入的文件名
        String InFileName = request.getInFileName();

        //输入的文件名
        String OutFileName = request.getOutFileName();

        //水印文件流
        ByteString wm_bys = request.getWaterMarkStream();
        byte[] wm_btyeArr = wm_bys.toByteArray();
        InputStream wm_inputStream = new ByteArrayInputStream(wm_btyeArr);

        //水印文件名
        String waterMarkFileName = request.getWaterMarkFileName();
        //水印文字
        String waterMarkText = request.getWaterMarkText();


        //读取配置文件
        Resource resource = new ClassPathResource("conf/FileMap.json");
        InputStream is = null;
        try {
            is = resource.getInputStream();
        } catch (IOException e) {
            e.printStackTrace();
        }
        Reader reader = new InputStreamReader(is, StandardCharsets.UTF_8);
        int ch = 0;
        StringBuffer sb = new StringBuffer();
        while (true) {
            try {
                if (!((ch = reader.read()) != -1)) break;
            } catch (IOException e) {
                e.printStackTrace();
            }
            sb.append((char) ch);
        }
        try {
            is.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        String jsonStr = sb.toString();
        HashMap<String, HashMap<ExtType, List<String>>> map = new Gson().fromJson(jsonStr, new TypeToken<HashMap<String, HashMap<ExtType, List<String>>>>() {
        }.getType());


        String InExt = FilenameUtils.getExtension(InFileName).toLowerCase();
        String OutExt = FilenameUtils.getExtension(OutFileName).toLowerCase();

        String ConvertClass = "Pdf";
        for (Map.Entry<String, HashMap<ExtType, List<String>>> entry : map.entrySet()) {

            HashMap<ExtType, List<String>> sub_map = entry.getValue();

            boolean infind_int = false;

            boolean outfind_int = false;
            List<String> InList = sub_map.get(ExtType.INPUTExt);
            List<String> OunList = sub_map.get(ExtType.OUTPUTExt);

            for (String Item : InList) {
                if (Item.toUpperCase().equals(InExt.toUpperCase())) {
                    infind_int = true;
                    break;
                }
            }
            for (String Item : OunList) {
                if (Item.toUpperCase().equals(OutExt.toUpperCase())) {
                    outfind_int = true;
                    break;
                }
            }
            if (infind_int && outfind_int) {
                ConvertClass = entry.getKey();
                break;
            }
        }
        Class<?> cls = null;
        try {
            cls = Class.forName("com.jillchen.grpc.server.convertprovidors.Convert" + ConvertClass);
        } catch (ClassNotFoundException e) {

            final ConvertStream.ResponseStreamReply.Builder replyBuilder = ConvertStream.ResponseStreamReply.newBuilder();
            replyBuilder.setMessages(String.format("不支持类型%s to %s", InExt, OutExt));
            replyBuilder.setStatus(-1);
            responseObserver.onNext(replyBuilder.build());
            responseObserver.onCompleted();
            return;
        }
        AbsConverter convert = new ConvertPdf();
        try {

            convert = (AbsConverter) cls.newInstance();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        int result = -1;
        ByteArrayOutputStream outStream = new ByteArrayOutputStream();
        //转换流
        result = convert.Convert(inputStream, InFileName, outStream, OutFileName);
        if (result != 0) {
            final ConvertStream.ResponseStreamReply.Builder replyBuilder = ConvertStream.ResponseStreamReply.newBuilder();
            replyBuilder.setMessages(String.format("从类型%s to %s转换失败", InExt, OutExt));
            replyBuilder.setStatus(-1);
            responseObserver.onNext(replyBuilder.build());
            responseObserver.onCompleted();
            return;
        }

        //判断水印流
        /*boolean w_file = false;
        try {
            new NonEmptyInputStream(wm_inputStream);
            w_file = true;
        } catch (IOException e) {

        }*/
        //不需要添加水印
        if("".equals(waterMarkFileName)&&"".equals(waterMarkText))
        {
            final ConvertStream.ResponseStreamReply.Builder replyBuilder = ConvertStream.ResponseStreamReply.newBuilder();
            replyBuilder.setMessages(String.format("%s to %s convert ok", InExt, OutExt));
            replyBuilder.setStatus(1);
            ByteString byteString = ByteString.copyFrom(outStream.toByteArray());//stream2ByteString
            replyBuilder.setOutputStream(byteString);
            responseObserver.onNext(replyBuilder.build());
            responseObserver.onCompleted();
            return;

        }

        InputStream waterMarkinputStream= TypeConverter.ByteArrayOutputStream2InputStream(outStream);
        ByteArrayOutputStream waterMarkoutStream = new ByteArrayOutputStream();
        result=-1;
        try {
            result=convert.WaterMark(waterMarkinputStream,OutFileName,wm_inputStream,waterMarkFileName,waterMarkText,waterMarkoutStream,OutFileName);
        } catch (Exception e) {

        }
        if(result!=0)
        {
            final ConvertStream.ResponseStreamReply.Builder replyBuilder = ConvertStream.ResponseStreamReply.newBuilder();
            replyBuilder.setMessages(String.format("从类型%s to %s转换失败", InExt, OutExt));
            replyBuilder.setStatus(-1);
            responseObserver.onNext(replyBuilder.build());
            responseObserver.onCompleted();
            return;
        }

        final ConvertStream.ResponseStreamReply.Builder replyBuilder1 = ConvertStream.ResponseStreamReply.newBuilder();
        replyBuilder1.setMessages(String.format("%s to %s convert ok", InExt, OutExt));
        replyBuilder1.setStatus(1);
        ByteString byteString = ByteString.copyFrom(outStream.toByteArray());//stream2ByteString
        replyBuilder1.setOutputStream(byteString);
        responseObserver.onNext(replyBuilder1.build());
        responseObserver.onCompleted();
        return;




    }


}
