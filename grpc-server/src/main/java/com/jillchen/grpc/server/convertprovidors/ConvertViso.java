package com.jillchen.grpc.server.convertprovidors;



import com.aspose.diagram.Diagram;
import com.aspose.diagram.HTMLSaveOptions;
import com.aspose.diagram.SaveFileFormat;
import com.aspose.diagram.SaveOptions;
import com.jillchen.grpc.server.utils.AbsConverter;
import org.apache.commons.io.FilenameUtils;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.util.Arrays;

public class ConvertViso extends AbsConverter {
    @Override
    public int Convert(String InFile,String OutFile)  {
        try {

            long old = System.currentTimeMillis();

            Diagram doc = new Diagram(InFile);

            int f= SaveFileFormat.PDF;
            String FileExt = FilenameUtils.getExtension(OutFile).toUpperCase();
            for (Field e : SaveFileFormat.class.getDeclaredFields()) {
                if(e.getName().toUpperCase().equals(FileExt))
                {
                    f= e.getInt(SaveFileFormat.class);
                }
            }
            if("HTML".equals(FileExt)) {
               //todo 补全viso转换html时需要把图片全部放入html中
                HTMLSaveOptions so=new HTMLSaveOptions();
                so.setSaveToolBar(false);
                
            }
            doc.save(OutFile, f);
            long now = System.currentTimeMillis();
            System.out.println("convert OK! " + ((now - old) / 1000.0) + "s");
            return 0;
        }
        catch (Exception ex)
        {
            System.out.println(ex.getMessage()+"\r\n"+ Arrays.toString(ex.getStackTrace()));
            return  -3;
        }
    }
    @Override
    public int Convert(InputStream inputStream, String InFile, ByteArrayOutputStream outputStream, String OutFile) {
        try {

            long old = System.currentTimeMillis();

            Diagram doc = new Diagram(inputStream);

            int f= SaveFileFormat.PDF;
            String FileExt = FilenameUtils.getExtension(OutFile).toUpperCase();
            for (Field e : SaveFileFormat.class.getDeclaredFields()) {
                if(e.getName().toUpperCase().equals(FileExt))
                {
                    f= e.getInt(SaveFileFormat.class);
                }
            }
            if("HTML".equals(FileExt)) {
                //todo 补全viso转换html时需要把图片全部放入html中
                HTMLSaveOptions so=new HTMLSaveOptions();
                so.setSaveToolBar(false);

            }
            doc.save(outputStream, f);
            long now = System.currentTimeMillis();
            System.out.println("convert OK! " + ((now - old) / 1000.0) + "s");
            return 0;
        }
        catch (Exception ex)
        {
            System.out.println(ex.getMessage()+"\r\n"+ Arrays.toString(ex.getStackTrace()));
            return  -3;
        }
    }
}
