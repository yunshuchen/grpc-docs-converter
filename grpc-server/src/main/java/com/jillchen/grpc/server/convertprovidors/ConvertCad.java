package com.jillchen.grpc.server.convertprovidors;




import com.aspose.cad.Color;
import com.aspose.cad.ImageOptionsBase;
import com.aspose.cad.imageoptions.*;
import com.jillchen.grpc.server.utils.AbsConverter;
import org.apache.commons.io.FilenameUtils;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Arrays;

public class ConvertCad extends AbsConverter {
    @Override
    public int Convert(String InFile,String OutFile)  {
        try {

            long old = System.currentTimeMillis();

            com.aspose.cad.Image doc = com.aspose.cad.Image.load(InFile);
            CadRasterizationOptions ratificationOptions = new CadRasterizationOptions();
            ratificationOptions.setBackgroundColor(Color.getWhite());
            ratificationOptions.setPageWidth(1600);
            ratificationOptions.setPageHeight(1600);
            String FileExt = FilenameUtils.getExtension(OutFile).toUpperCase();
            if("BMP".equals(FileExt)) {
                BmpOptions bmpOptions = new BmpOptions();
                bmpOptions.setVectorRasterizationOptions(ratificationOptions);
                doc.save(OutFile, bmpOptions);
            }else if("JPG".equals(FileExt)||"JPEG".equals(FileExt)) {
                JpegOptions bmpOptions = new JpegOptions();
                bmpOptions.setVectorRasterizationOptions(ratificationOptions);
                doc.save(OutFile, bmpOptions);
            }else if("PNG".equals(FileExt)) {
                PngOptions bmpOptions = new PngOptions();
                bmpOptions.setVectorRasterizationOptions(ratificationOptions);
                doc.save(OutFile, bmpOptions);
            }else if("GIF".equals(FileExt)) {
                GifOptions bmpOptions = new GifOptions();
                bmpOptions.setVectorRasterizationOptions(ratificationOptions);
                doc.save(OutFile, bmpOptions);
            }
            else if("WMF".equals(FileExt)){
                WmfOptions wmfOptions = new WmfOptions();
                wmfOptions.setVectorRasterizationOptions(ratificationOptions);
                doc.save(OutFile, wmfOptions);
            }
            else {
                PdfOptions pdfOptions = new PdfOptions();
                pdfOptions.setVectorRasterizationOptions(ratificationOptions);
                doc.save(OutFile, pdfOptions);
            }

            long now = System.currentTimeMillis();
            System.out.println("convert OK! " + ((now - old) / 1000.0) + "s");
            return  0;
        } catch (Exception ex) {
            System.out.println(ex.getMessage()+"\r\n"+ Arrays.toString(ex.getStackTrace()));
            return  -3;
        }
    }

    @Override
    public int Convert(InputStream inputStream, String InFile, ByteArrayOutputStream outputStream, String OutFile) {
        try {

            long old = System.currentTimeMillis();

            com.aspose.cad.Image doc = com.aspose.cad.Image.load(inputStream);
            CadRasterizationOptions ratificationOptions = new CadRasterizationOptions();
            ratificationOptions.setBackgroundColor(Color.getWhite());
            ratificationOptions.setPageWidth(1600);
            ratificationOptions.setPageHeight(1600);
            String FileExt = FilenameUtils.getExtension(OutFile).toUpperCase();
            if("BMP".equals(FileExt)) {
                BmpOptions bmpOptions = new BmpOptions();
                bmpOptions.setVectorRasterizationOptions(ratificationOptions);
                doc.save(outputStream, bmpOptions);
            }else if("JPG".equals(FileExt)||"JPEG".equals(FileExt)) {
                JpegOptions bmpOptions = new JpegOptions();
                bmpOptions.setVectorRasterizationOptions(ratificationOptions);
                doc.save(outputStream, bmpOptions);
            }else if("PNG".equals(FileExt)) {
                PngOptions bmpOptions = new PngOptions();
                bmpOptions.setVectorRasterizationOptions(ratificationOptions);
                doc.save(outputStream, bmpOptions);
            }else if("GIF".equals(FileExt)) {
                GifOptions bmpOptions = new GifOptions();
                bmpOptions.setVectorRasterizationOptions(ratificationOptions);
                doc.save(outputStream, bmpOptions);
            }
            else if("WMF".equals(FileExt)){
                WmfOptions wmfOptions = new WmfOptions();
                wmfOptions.setVectorRasterizationOptions(ratificationOptions);
                doc.save(outputStream, wmfOptions);
            }
            else {
                PdfOptions pdfOptions = new PdfOptions();
                pdfOptions.setVectorRasterizationOptions(ratificationOptions);
                doc.save(outputStream, pdfOptions);
            }

            long now = System.currentTimeMillis();
            System.out.println("convert OK! " + ((now - old) / 1000.0) + "s");
            return  0;
        } catch (Exception ex) {
            System.out.println(ex.getMessage()+"\r\n"+ Arrays.toString(ex.getStackTrace()));
            return  -3;
        }
    }
}
