package com.jillchen.grpc.server.convertprovidors;



import com.aspose.psd.Image;
import com.aspose.psd.LoadOptions;
import com.aspose.psd.fileformats.psd.PsdImage;
import com.aspose.psd.fileformats.tiff.enums.TiffExpectedFormat;
import com.aspose.psd.imageloadoptions.PsdLoadOptions;
import com.aspose.psd.imageoptions.*;
import com.jillchen.grpc.server.utils.AbsConverter;
import org.apache.commons.io.FilenameUtils;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Arrays;

public class ConvertPsd  extends AbsConverter {

    @Override
    public int Convert(String inPath,String outPath)  {

        try {

            long old = System.currentTimeMillis();
            File file = new File(outPath);
            FileOutputStream os = new FileOutputStream(file);
            String InFileExt = FilenameUtils.getExtension(inPath).toUpperCase();

            PsdImage image = (PsdImage)Image.load(inPath);

            String FileExt = FilenameUtils.getExtension(outPath).toUpperCase();
            if(FileExt.equals("PDF")) {

                image.save(outPath, new PdfOptions());
            }else if(FileExt.equals("TIFF"))
            {
                image.save(outPath, new TiffOptions(TiffExpectedFormat.TiffLzwCmyk));
            }else if(FileExt.equals("JPG")||FileExt.equals("JPEG"))
            {
                image.save(outPath, new JpegOptions());
            }else if(FileExt.equals("PNG"))
            {
                image.save(outPath, new PngOptions());
            }else if(FileExt.equals("BMP"))
            {
                image.save(outPath, new BmpOptions());
            }else if(FileExt.equals("GIF"))
            {
                image.save(outPath, new GifOptions());
            }else
            {
                image.save(outPath, new Jpeg2000Options());
            }
            long now = System.currentTimeMillis();
            System.out.println("convert OK! " + ((now - old) / 1000.0) + "s");
            return 0;
        }
        catch (Exception ex)
        {
            System.out.println(ex.getMessage()+"\r\n"+ Arrays.toString(ex.getStackTrace()));
            return  -3;
        }
    }

    @Override
    public int Convert(InputStream inputStream, String InFile, ByteArrayOutputStream outputStream, String OutFile) {

        try {

            long old = System.currentTimeMillis();

            String InFileExt = FilenameUtils.getExtension(InFile).toUpperCase();
            LoadOptions opetion=new PsdLoadOptions();

            PsdImage image = (PsdImage)Image.load(inputStream,opetion);

            String FileExt = FilenameUtils.getExtension(OutFile).toUpperCase();
            if(FileExt.equals("PDF")) {

                image.save(outputStream, new PdfOptions());
            }else if(FileExt.equals("TIFF"))
            {
                image.save(outputStream, new TiffOptions(TiffExpectedFormat.TiffLzwCmyk));
            }else if(FileExt.equals("JPG")||FileExt.equals("JPEG"))
            {
                image.save(outputStream, new JpegOptions());
            }else if(FileExt.equals("PNG"))
            {
                image.save(outputStream, new PngOptions());
            }else if(FileExt.equals("BMP"))
            {
                image.save(outputStream, new BmpOptions());
            }else if(FileExt.equals("GIF"))
            {
                image.save(outputStream, new GifOptions());
            }else
            {
                image.save(outputStream, new Jpeg2000Options());
            }
            long now = System.currentTimeMillis();
            System.out.println("convert OK! " + ((now - old) / 1000.0) + "s");
            return 0;
        }
        catch (Exception ex)
        {
            System.out.println(ex.getMessage()+"\r\n"+ Arrays.toString(ex.getStackTrace()));
            return  -3;
        }
    }
}
