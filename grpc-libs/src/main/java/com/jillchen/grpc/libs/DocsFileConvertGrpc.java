package com.jillchen.grpc.libs;

import static io.grpc.MethodDescriptor.generateFullMethodName;
import static io.grpc.stub.ClientCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ClientCalls.asyncClientStreamingCall;
import static io.grpc.stub.ClientCalls.asyncServerStreamingCall;
import static io.grpc.stub.ClientCalls.asyncUnaryCall;
import static io.grpc.stub.ClientCalls.blockingServerStreamingCall;
import static io.grpc.stub.ClientCalls.blockingUnaryCall;
import static io.grpc.stub.ClientCalls.futureUnaryCall;
import static io.grpc.stub.ServerCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ServerCalls.asyncClientStreamingCall;
import static io.grpc.stub.ServerCalls.asyncServerStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnaryCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall;

/**
 * <pre>
 * The greeting service definition.
 * </pre>
 */
@javax.annotation.Generated(
    value = "by gRPC proto compiler (version 1.15.1)",
    comments = "Source: ConvertFile.proto")
public final class DocsFileConvertGrpc {

  private DocsFileConvertGrpc() {}

  public static final String SERVICE_NAME = "docsFile.DocsFileConvert";

  // Static method descriptors that strictly reflect the proto.
  private static volatile io.grpc.MethodDescriptor<com.jillchen.grpc.libs.ConvertFile.RequestConvertFile,
      com.jillchen.grpc.libs.ConvertFile.ResponseFileConvertReply> getConvertMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "Convert",
      requestType = com.jillchen.grpc.libs.ConvertFile.RequestConvertFile.class,
      responseType = com.jillchen.grpc.libs.ConvertFile.ResponseFileConvertReply.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<com.jillchen.grpc.libs.ConvertFile.RequestConvertFile,
      com.jillchen.grpc.libs.ConvertFile.ResponseFileConvertReply> getConvertMethod() {
    io.grpc.MethodDescriptor<com.jillchen.grpc.libs.ConvertFile.RequestConvertFile, com.jillchen.grpc.libs.ConvertFile.ResponseFileConvertReply> getConvertMethod;
    if ((getConvertMethod = DocsFileConvertGrpc.getConvertMethod) == null) {
      synchronized (DocsFileConvertGrpc.class) {
        if ((getConvertMethod = DocsFileConvertGrpc.getConvertMethod) == null) {
          DocsFileConvertGrpc.getConvertMethod = getConvertMethod = 
              io.grpc.MethodDescriptor.<com.jillchen.grpc.libs.ConvertFile.RequestConvertFile, com.jillchen.grpc.libs.ConvertFile.ResponseFileConvertReply>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(
                  "docsFile.DocsFileConvert", "Convert"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.jillchen.grpc.libs.ConvertFile.RequestConvertFile.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.jillchen.grpc.libs.ConvertFile.ResponseFileConvertReply.getDefaultInstance()))
                  .setSchemaDescriptor(new DocsFileConvertMethodDescriptorSupplier("Convert"))
                  .build();
          }
        }
     }
     return getConvertMethod;
  }

  /**
   * Creates a new async stub that supports all call types for the service
   */
  public static DocsFileConvertStub newStub(io.grpc.Channel channel) {
    return new DocsFileConvertStub(channel);
  }

  /**
   * Creates a new blocking-style stub that supports unary and streaming output calls on the service
   */
  public static DocsFileConvertBlockingStub newBlockingStub(
      io.grpc.Channel channel) {
    return new DocsFileConvertBlockingStub(channel);
  }

  /**
   * Creates a new ListenableFuture-style stub that supports unary calls on the service
   */
  public static DocsFileConvertFutureStub newFutureStub(
      io.grpc.Channel channel) {
    return new DocsFileConvertFutureStub(channel);
  }

  /**
   * <pre>
   * The greeting service definition.
   * </pre>
   */
  public static abstract class DocsFileConvertImplBase implements io.grpc.BindableService {

    /**
     * <pre>
     * Sends a greeting
     * </pre>
     */
    public void convert(com.jillchen.grpc.libs.ConvertFile.RequestConvertFile request,
        io.grpc.stub.StreamObserver<com.jillchen.grpc.libs.ConvertFile.ResponseFileConvertReply> responseObserver) {
      asyncUnimplementedUnaryCall(getConvertMethod(), responseObserver);
    }

    @java.lang.Override public final io.grpc.ServerServiceDefinition bindService() {
      return io.grpc.ServerServiceDefinition.builder(getServiceDescriptor())
          .addMethod(
            getConvertMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                com.jillchen.grpc.libs.ConvertFile.RequestConvertFile,
                com.jillchen.grpc.libs.ConvertFile.ResponseFileConvertReply>(
                  this, METHODID_CONVERT)))
          .build();
    }
  }

  /**
   * <pre>
   * The greeting service definition.
   * </pre>
   */
  public static final class DocsFileConvertStub extends io.grpc.stub.AbstractStub<DocsFileConvertStub> {
    private DocsFileConvertStub(io.grpc.Channel channel) {
      super(channel);
    }

    private DocsFileConvertStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected DocsFileConvertStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new DocsFileConvertStub(channel, callOptions);
    }

    /**
     * <pre>
     * Sends a greeting
     * </pre>
     */
    public void convert(com.jillchen.grpc.libs.ConvertFile.RequestConvertFile request,
        io.grpc.stub.StreamObserver<com.jillchen.grpc.libs.ConvertFile.ResponseFileConvertReply> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getConvertMethod(), getCallOptions()), request, responseObserver);
    }
  }

  /**
   * <pre>
   * The greeting service definition.
   * </pre>
   */
  public static final class DocsFileConvertBlockingStub extends io.grpc.stub.AbstractStub<DocsFileConvertBlockingStub> {
    private DocsFileConvertBlockingStub(io.grpc.Channel channel) {
      super(channel);
    }

    private DocsFileConvertBlockingStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected DocsFileConvertBlockingStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new DocsFileConvertBlockingStub(channel, callOptions);
    }

    /**
     * <pre>
     * Sends a greeting
     * </pre>
     */
    public com.jillchen.grpc.libs.ConvertFile.ResponseFileConvertReply convert(com.jillchen.grpc.libs.ConvertFile.RequestConvertFile request) {
      return blockingUnaryCall(
          getChannel(), getConvertMethod(), getCallOptions(), request);
    }
  }

  /**
   * <pre>
   * The greeting service definition.
   * </pre>
   */
  public static final class DocsFileConvertFutureStub extends io.grpc.stub.AbstractStub<DocsFileConvertFutureStub> {
    private DocsFileConvertFutureStub(io.grpc.Channel channel) {
      super(channel);
    }

    private DocsFileConvertFutureStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected DocsFileConvertFutureStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new DocsFileConvertFutureStub(channel, callOptions);
    }

    /**
     * <pre>
     * Sends a greeting
     * </pre>
     */
    public com.google.common.util.concurrent.ListenableFuture<com.jillchen.grpc.libs.ConvertFile.ResponseFileConvertReply> convert(
        com.jillchen.grpc.libs.ConvertFile.RequestConvertFile request) {
      return futureUnaryCall(
          getChannel().newCall(getConvertMethod(), getCallOptions()), request);
    }
  }

  private static final int METHODID_CONVERT = 0;

  private static final class MethodHandlers<Req, Resp> implements
      io.grpc.stub.ServerCalls.UnaryMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ServerStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ClientStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.BidiStreamingMethod<Req, Resp> {
    private final DocsFileConvertImplBase serviceImpl;
    private final int methodId;

    MethodHandlers(DocsFileConvertImplBase serviceImpl, int methodId) {
      this.serviceImpl = serviceImpl;
      this.methodId = methodId;
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public void invoke(Req request, io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        case METHODID_CONVERT:
          serviceImpl.convert((com.jillchen.grpc.libs.ConvertFile.RequestConvertFile) request,
              (io.grpc.stub.StreamObserver<com.jillchen.grpc.libs.ConvertFile.ResponseFileConvertReply>) responseObserver);
          break;
        default:
          throw new AssertionError();
      }
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public io.grpc.stub.StreamObserver<Req> invoke(
        io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        default:
          throw new AssertionError();
      }
    }
  }

  private static abstract class DocsFileConvertBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoFileDescriptorSupplier, io.grpc.protobuf.ProtoServiceDescriptorSupplier {
    DocsFileConvertBaseDescriptorSupplier() {}

    @java.lang.Override
    public com.google.protobuf.Descriptors.FileDescriptor getFileDescriptor() {
      return com.jillchen.grpc.libs.ConvertFile.getDescriptor();
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.ServiceDescriptor getServiceDescriptor() {
      return getFileDescriptor().findServiceByName("DocsFileConvert");
    }
  }

  private static final class DocsFileConvertFileDescriptorSupplier
      extends DocsFileConvertBaseDescriptorSupplier {
    DocsFileConvertFileDescriptorSupplier() {}
  }

  private static final class DocsFileConvertMethodDescriptorSupplier
      extends DocsFileConvertBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoMethodDescriptorSupplier {
    private final String methodName;

    DocsFileConvertMethodDescriptorSupplier(String methodName) {
      this.methodName = methodName;
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.MethodDescriptor getMethodDescriptor() {
      return getServiceDescriptor().findMethodByName(methodName);
    }
  }

  private static volatile io.grpc.ServiceDescriptor serviceDescriptor;

  public static io.grpc.ServiceDescriptor getServiceDescriptor() {
    io.grpc.ServiceDescriptor result = serviceDescriptor;
    if (result == null) {
      synchronized (DocsFileConvertGrpc.class) {
        result = serviceDescriptor;
        if (result == null) {
          serviceDescriptor = result = io.grpc.ServiceDescriptor.newBuilder(SERVICE_NAME)
              .setSchemaDescriptor(new DocsFileConvertFileDescriptorSupplier())
              .addMethod(getConvertMethod())
              .build();
        }
      }
    }
    return result;
  }
}
