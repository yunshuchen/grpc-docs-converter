package com.jillchen.grpc.libs;

import static io.grpc.MethodDescriptor.generateFullMethodName;
import static io.grpc.stub.ClientCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ClientCalls.asyncClientStreamingCall;
import static io.grpc.stub.ClientCalls.asyncServerStreamingCall;
import static io.grpc.stub.ClientCalls.asyncUnaryCall;
import static io.grpc.stub.ClientCalls.blockingServerStreamingCall;
import static io.grpc.stub.ClientCalls.blockingUnaryCall;
import static io.grpc.stub.ClientCalls.futureUnaryCall;
import static io.grpc.stub.ServerCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ServerCalls.asyncClientStreamingCall;
import static io.grpc.stub.ServerCalls.asyncServerStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnaryCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall;

/**
 * <pre>
 * The greeting service definition.
 * </pre>
 */
@javax.annotation.Generated(
    value = "by gRPC proto compiler (version 1.15.1)",
    comments = "Source: ConvertStream.proto")
public final class DocsStreamConvertGrpc {

  private DocsStreamConvertGrpc() {}

  public static final String SERVICE_NAME = "docsStream.DocsStreamConvert";

  // Static method descriptors that strictly reflect the proto.
  private static volatile io.grpc.MethodDescriptor<com.jillchen.grpc.libs.ConvertStream.RequestInputFileStream,
      com.jillchen.grpc.libs.ConvertStream.ResponseStreamReply> getConvertMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "Convert",
      requestType = com.jillchen.grpc.libs.ConvertStream.RequestInputFileStream.class,
      responseType = com.jillchen.grpc.libs.ConvertStream.ResponseStreamReply.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<com.jillchen.grpc.libs.ConvertStream.RequestInputFileStream,
      com.jillchen.grpc.libs.ConvertStream.ResponseStreamReply> getConvertMethod() {
    io.grpc.MethodDescriptor<com.jillchen.grpc.libs.ConvertStream.RequestInputFileStream, com.jillchen.grpc.libs.ConvertStream.ResponseStreamReply> getConvertMethod;
    if ((getConvertMethod = DocsStreamConvertGrpc.getConvertMethod) == null) {
      synchronized (DocsStreamConvertGrpc.class) {
        if ((getConvertMethod = DocsStreamConvertGrpc.getConvertMethod) == null) {
          DocsStreamConvertGrpc.getConvertMethod = getConvertMethod = 
              io.grpc.MethodDescriptor.<com.jillchen.grpc.libs.ConvertStream.RequestInputFileStream, com.jillchen.grpc.libs.ConvertStream.ResponseStreamReply>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(
                  "docsStream.DocsStreamConvert", "Convert"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.jillchen.grpc.libs.ConvertStream.RequestInputFileStream.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.jillchen.grpc.libs.ConvertStream.ResponseStreamReply.getDefaultInstance()))
                  .setSchemaDescriptor(new DocsStreamConvertMethodDescriptorSupplier("Convert"))
                  .build();
          }
        }
     }
     return getConvertMethod;
  }

  /**
   * Creates a new async stub that supports all call types for the service
   */
  public static DocsStreamConvertStub newStub(io.grpc.Channel channel) {
    return new DocsStreamConvertStub(channel);
  }

  /**
   * Creates a new blocking-style stub that supports unary and streaming output calls on the service
   */
  public static DocsStreamConvertBlockingStub newBlockingStub(
      io.grpc.Channel channel) {
    return new DocsStreamConvertBlockingStub(channel);
  }

  /**
   * Creates a new ListenableFuture-style stub that supports unary calls on the service
   */
  public static DocsStreamConvertFutureStub newFutureStub(
      io.grpc.Channel channel) {
    return new DocsStreamConvertFutureStub(channel);
  }

  /**
   * <pre>
   * The greeting service definition.
   * </pre>
   */
  public static abstract class DocsStreamConvertImplBase implements io.grpc.BindableService {

    /**
     * <pre>
     * Sends a greeting
     * </pre>
     */
    public void convert(com.jillchen.grpc.libs.ConvertStream.RequestInputFileStream request,
        io.grpc.stub.StreamObserver<com.jillchen.grpc.libs.ConvertStream.ResponseStreamReply> responseObserver) {
      asyncUnimplementedUnaryCall(getConvertMethod(), responseObserver);
    }

    @java.lang.Override public final io.grpc.ServerServiceDefinition bindService() {
      return io.grpc.ServerServiceDefinition.builder(getServiceDescriptor())
          .addMethod(
            getConvertMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                com.jillchen.grpc.libs.ConvertStream.RequestInputFileStream,
                com.jillchen.grpc.libs.ConvertStream.ResponseStreamReply>(
                  this, METHODID_CONVERT)))
          .build();
    }
  }

  /**
   * <pre>
   * The greeting service definition.
   * </pre>
   */
  public static final class DocsStreamConvertStub extends io.grpc.stub.AbstractStub<DocsStreamConvertStub> {
    private DocsStreamConvertStub(io.grpc.Channel channel) {
      super(channel);
    }

    private DocsStreamConvertStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected DocsStreamConvertStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new DocsStreamConvertStub(channel, callOptions);
    }

    /**
     * <pre>
     * Sends a greeting
     * </pre>
     */
    public void convert(com.jillchen.grpc.libs.ConvertStream.RequestInputFileStream request,
        io.grpc.stub.StreamObserver<com.jillchen.grpc.libs.ConvertStream.ResponseStreamReply> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getConvertMethod(), getCallOptions()), request, responseObserver);
    }
  }

  /**
   * <pre>
   * The greeting service definition.
   * </pre>
   */
  public static final class DocsStreamConvertBlockingStub extends io.grpc.stub.AbstractStub<DocsStreamConvertBlockingStub> {
    private DocsStreamConvertBlockingStub(io.grpc.Channel channel) {
      super(channel);
    }

    private DocsStreamConvertBlockingStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected DocsStreamConvertBlockingStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new DocsStreamConvertBlockingStub(channel, callOptions);
    }

    /**
     * <pre>
     * Sends a greeting
     * </pre>
     */
    public com.jillchen.grpc.libs.ConvertStream.ResponseStreamReply convert(com.jillchen.grpc.libs.ConvertStream.RequestInputFileStream request) {
      return blockingUnaryCall(
          getChannel(), getConvertMethod(), getCallOptions(), request);
    }
  }

  /**
   * <pre>
   * The greeting service definition.
   * </pre>
   */
  public static final class DocsStreamConvertFutureStub extends io.grpc.stub.AbstractStub<DocsStreamConvertFutureStub> {
    private DocsStreamConvertFutureStub(io.grpc.Channel channel) {
      super(channel);
    }

    private DocsStreamConvertFutureStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected DocsStreamConvertFutureStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new DocsStreamConvertFutureStub(channel, callOptions);
    }

    /**
     * <pre>
     * Sends a greeting
     * </pre>
     */
    public com.google.common.util.concurrent.ListenableFuture<com.jillchen.grpc.libs.ConvertStream.ResponseStreamReply> convert(
        com.jillchen.grpc.libs.ConvertStream.RequestInputFileStream request) {
      return futureUnaryCall(
          getChannel().newCall(getConvertMethod(), getCallOptions()), request);
    }
  }

  private static final int METHODID_CONVERT = 0;

  private static final class MethodHandlers<Req, Resp> implements
      io.grpc.stub.ServerCalls.UnaryMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ServerStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ClientStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.BidiStreamingMethod<Req, Resp> {
    private final DocsStreamConvertImplBase serviceImpl;
    private final int methodId;

    MethodHandlers(DocsStreamConvertImplBase serviceImpl, int methodId) {
      this.serviceImpl = serviceImpl;
      this.methodId = methodId;
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public void invoke(Req request, io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        case METHODID_CONVERT:
          serviceImpl.convert((com.jillchen.grpc.libs.ConvertStream.RequestInputFileStream) request,
              (io.grpc.stub.StreamObserver<com.jillchen.grpc.libs.ConvertStream.ResponseStreamReply>) responseObserver);
          break;
        default:
          throw new AssertionError();
      }
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public io.grpc.stub.StreamObserver<Req> invoke(
        io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        default:
          throw new AssertionError();
      }
    }
  }

  private static abstract class DocsStreamConvertBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoFileDescriptorSupplier, io.grpc.protobuf.ProtoServiceDescriptorSupplier {
    DocsStreamConvertBaseDescriptorSupplier() {}

    @java.lang.Override
    public com.google.protobuf.Descriptors.FileDescriptor getFileDescriptor() {
      return com.jillchen.grpc.libs.ConvertStream.getDescriptor();
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.ServiceDescriptor getServiceDescriptor() {
      return getFileDescriptor().findServiceByName("DocsStreamConvert");
    }
  }

  private static final class DocsStreamConvertFileDescriptorSupplier
      extends DocsStreamConvertBaseDescriptorSupplier {
    DocsStreamConvertFileDescriptorSupplier() {}
  }

  private static final class DocsStreamConvertMethodDescriptorSupplier
      extends DocsStreamConvertBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoMethodDescriptorSupplier {
    private final String methodName;

    DocsStreamConvertMethodDescriptorSupplier(String methodName) {
      this.methodName = methodName;
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.MethodDescriptor getMethodDescriptor() {
      return getServiceDescriptor().findMethodByName(methodName);
    }
  }

  private static volatile io.grpc.ServiceDescriptor serviceDescriptor;

  public static io.grpc.ServiceDescriptor getServiceDescriptor() {
    io.grpc.ServiceDescriptor result = serviceDescriptor;
    if (result == null) {
      synchronized (DocsStreamConvertGrpc.class) {
        result = serviceDescriptor;
        if (result == null) {
          serviceDescriptor = result = io.grpc.ServiceDescriptor.newBuilder(SERVICE_NAME)
              .setSchemaDescriptor(new DocsStreamConvertFileDescriptorSupplier())
              .addMethod(getConvertMethod())
              .build();
        }
      }
    }
    return result;
  }
}
