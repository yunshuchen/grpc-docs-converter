package com.jillchen.grpc.libs;

import static io.grpc.MethodDescriptor.generateFullMethodName;
import static io.grpc.stub.ClientCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ClientCalls.asyncClientStreamingCall;
import static io.grpc.stub.ClientCalls.asyncServerStreamingCall;
import static io.grpc.stub.ClientCalls.asyncUnaryCall;
import static io.grpc.stub.ClientCalls.blockingServerStreamingCall;
import static io.grpc.stub.ClientCalls.blockingUnaryCall;
import static io.grpc.stub.ClientCalls.futureUnaryCall;
import static io.grpc.stub.ServerCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ServerCalls.asyncClientStreamingCall;
import static io.grpc.stub.ServerCalls.asyncServerStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnaryCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall;

/**
 */
@javax.annotation.Generated(
    value = "by gRPC proto compiler (version 1.15.1)",
    comments = "Source: InterruptStatus.proto")
public final class InterruptGrpc {

  private InterruptGrpc() {}

  public static final String SERVICE_NAME = "Interrupt";

  // Static method descriptors that strictly reflect the proto.
  private static volatile io.grpc.MethodDescriptor<com.jillchen.grpc.libs.InterruptStatus.RequestParam,
      com.jillchen.grpc.libs.InterruptStatus.Message> getGetInterruptStatusDataMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "getInterruptStatusData",
      requestType = com.jillchen.grpc.libs.InterruptStatus.RequestParam.class,
      responseType = com.jillchen.grpc.libs.InterruptStatus.Message.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<com.jillchen.grpc.libs.InterruptStatus.RequestParam,
      com.jillchen.grpc.libs.InterruptStatus.Message> getGetInterruptStatusDataMethod() {
    io.grpc.MethodDescriptor<com.jillchen.grpc.libs.InterruptStatus.RequestParam, com.jillchen.grpc.libs.InterruptStatus.Message> getGetInterruptStatusDataMethod;
    if ((getGetInterruptStatusDataMethod = InterruptGrpc.getGetInterruptStatusDataMethod) == null) {
      synchronized (InterruptGrpc.class) {
        if ((getGetInterruptStatusDataMethod = InterruptGrpc.getGetInterruptStatusDataMethod) == null) {
          InterruptGrpc.getGetInterruptStatusDataMethod = getGetInterruptStatusDataMethod = 
              io.grpc.MethodDescriptor.<com.jillchen.grpc.libs.InterruptStatus.RequestParam, com.jillchen.grpc.libs.InterruptStatus.Message>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(
                  "Interrupt", "getInterruptStatusData"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.jillchen.grpc.libs.InterruptStatus.RequestParam.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.jillchen.grpc.libs.InterruptStatus.Message.getDefaultInstance()))
                  .setSchemaDescriptor(new InterruptMethodDescriptorSupplier("getInterruptStatusData"))
                  .build();
          }
        }
     }
     return getGetInterruptStatusDataMethod;
  }

  /**
   * Creates a new async stub that supports all call types for the service
   */
  public static InterruptStub newStub(io.grpc.Channel channel) {
    return new InterruptStub(channel);
  }

  /**
   * Creates a new blocking-style stub that supports unary and streaming output calls on the service
   */
  public static InterruptBlockingStub newBlockingStub(
      io.grpc.Channel channel) {
    return new InterruptBlockingStub(channel);
  }

  /**
   * Creates a new ListenableFuture-style stub that supports unary calls on the service
   */
  public static InterruptFutureStub newFutureStub(
      io.grpc.Channel channel) {
    return new InterruptFutureStub(channel);
  }

  /**
   */
  public static abstract class InterruptImplBase implements io.grpc.BindableService {

    /**
     */
    public void getInterruptStatusData(com.jillchen.grpc.libs.InterruptStatus.RequestParam request,
        io.grpc.stub.StreamObserver<com.jillchen.grpc.libs.InterruptStatus.Message> responseObserver) {
      asyncUnimplementedUnaryCall(getGetInterruptStatusDataMethod(), responseObserver);
    }

    @java.lang.Override public final io.grpc.ServerServiceDefinition bindService() {
      return io.grpc.ServerServiceDefinition.builder(getServiceDescriptor())
          .addMethod(
            getGetInterruptStatusDataMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                com.jillchen.grpc.libs.InterruptStatus.RequestParam,
                com.jillchen.grpc.libs.InterruptStatus.Message>(
                  this, METHODID_GET_INTERRUPT_STATUS_DATA)))
          .build();
    }
  }

  /**
   */
  public static final class InterruptStub extends io.grpc.stub.AbstractStub<InterruptStub> {
    private InterruptStub(io.grpc.Channel channel) {
      super(channel);
    }

    private InterruptStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected InterruptStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new InterruptStub(channel, callOptions);
    }

    /**
     */
    public void getInterruptStatusData(com.jillchen.grpc.libs.InterruptStatus.RequestParam request,
        io.grpc.stub.StreamObserver<com.jillchen.grpc.libs.InterruptStatus.Message> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getGetInterruptStatusDataMethod(), getCallOptions()), request, responseObserver);
    }
  }

  /**
   */
  public static final class InterruptBlockingStub extends io.grpc.stub.AbstractStub<InterruptBlockingStub> {
    private InterruptBlockingStub(io.grpc.Channel channel) {
      super(channel);
    }

    private InterruptBlockingStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected InterruptBlockingStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new InterruptBlockingStub(channel, callOptions);
    }

    /**
     */
    public com.jillchen.grpc.libs.InterruptStatus.Message getInterruptStatusData(com.jillchen.grpc.libs.InterruptStatus.RequestParam request) {
      return blockingUnaryCall(
          getChannel(), getGetInterruptStatusDataMethod(), getCallOptions(), request);
    }
  }

  /**
   */
  public static final class InterruptFutureStub extends io.grpc.stub.AbstractStub<InterruptFutureStub> {
    private InterruptFutureStub(io.grpc.Channel channel) {
      super(channel);
    }

    private InterruptFutureStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected InterruptFutureStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new InterruptFutureStub(channel, callOptions);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<com.jillchen.grpc.libs.InterruptStatus.Message> getInterruptStatusData(
        com.jillchen.grpc.libs.InterruptStatus.RequestParam request) {
      return futureUnaryCall(
          getChannel().newCall(getGetInterruptStatusDataMethod(), getCallOptions()), request);
    }
  }

  private static final int METHODID_GET_INTERRUPT_STATUS_DATA = 0;

  private static final class MethodHandlers<Req, Resp> implements
      io.grpc.stub.ServerCalls.UnaryMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ServerStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ClientStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.BidiStreamingMethod<Req, Resp> {
    private final InterruptImplBase serviceImpl;
    private final int methodId;

    MethodHandlers(InterruptImplBase serviceImpl, int methodId) {
      this.serviceImpl = serviceImpl;
      this.methodId = methodId;
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public void invoke(Req request, io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        case METHODID_GET_INTERRUPT_STATUS_DATA:
          serviceImpl.getInterruptStatusData((com.jillchen.grpc.libs.InterruptStatus.RequestParam) request,
              (io.grpc.stub.StreamObserver<com.jillchen.grpc.libs.InterruptStatus.Message>) responseObserver);
          break;
        default:
          throw new AssertionError();
      }
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public io.grpc.stub.StreamObserver<Req> invoke(
        io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        default:
          throw new AssertionError();
      }
    }
  }

  private static abstract class InterruptBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoFileDescriptorSupplier, io.grpc.protobuf.ProtoServiceDescriptorSupplier {
    InterruptBaseDescriptorSupplier() {}

    @java.lang.Override
    public com.google.protobuf.Descriptors.FileDescriptor getFileDescriptor() {
      return com.jillchen.grpc.libs.InterruptStatus.getDescriptor();
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.ServiceDescriptor getServiceDescriptor() {
      return getFileDescriptor().findServiceByName("Interrupt");
    }
  }

  private static final class InterruptFileDescriptorSupplier
      extends InterruptBaseDescriptorSupplier {
    InterruptFileDescriptorSupplier() {}
  }

  private static final class InterruptMethodDescriptorSupplier
      extends InterruptBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoMethodDescriptorSupplier {
    private final String methodName;

    InterruptMethodDescriptorSupplier(String methodName) {
      this.methodName = methodName;
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.MethodDescriptor getMethodDescriptor() {
      return getServiceDescriptor().findMethodByName(methodName);
    }
  }

  private static volatile io.grpc.ServiceDescriptor serviceDescriptor;

  public static io.grpc.ServiceDescriptor getServiceDescriptor() {
    io.grpc.ServiceDescriptor result = serviceDescriptor;
    if (result == null) {
      synchronized (InterruptGrpc.class) {
        result = serviceDescriptor;
        if (result == null) {
          serviceDescriptor = result = io.grpc.ServiceDescriptor.newBuilder(SERVICE_NAME)
              .setSchemaDescriptor(new InterruptFileDescriptorSupplier())
              .addMethod(getGetInterruptStatusDataMethod())
              .build();
        }
      }
    }
    return result;
  }
}
